const express =  require('express');
const connectDB = require('./config/db');


const app = express();

//connect database

connectDB();
//Init Middleware

app.use(express.json({extended:false}));

app.get('/',(req,res)=> res.send(`API running`));

// Define Routes
app.use('/api/users',require('./routes/api/user'));
app.use('/api/auth',require('./routes/api/auth'));
app.use('/api/bus',require('./routes/api/bus'));
app.use('/api/admin',require('./routes/api/admin'));
app.use('/api/ticket',require("./routes/api/ticket"));


const PORT = process.env.PORT || 8000;

app.listen(PORT,()=>console.log(`Server started on port ${8000}`));