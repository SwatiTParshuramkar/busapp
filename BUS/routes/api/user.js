const express = require("express");
const router = express.Router();
const config = require('config');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { check, validationResult } = require('express-validator');
const User = require("../../models/User");

//@routes POST api/users
//@desc Register user
//@access Public

router.post('/',[
    check('name','Name is required').notEmpty(),
    check('gender','gender name is required').notEmpty(),
    check('age','age is required').notEmpty(),
    check('email','email is required').isEmail(),
    check('phoneNumber','Phone Number is required'),
    check('password','password can be 6 or 8 character').isLength({min:6}),
    check('isAdmin','isAdmin check it is').notEmpty(),
],async(req,res)=> {
    const errors = validationResult(req); 
    if(!errors.isEmpty()){
        return res.status(400).json({errors:errors.array()})
    }

    const {name,gender,age,email,password,phoneNumber,isAdmin} = req.body;

    try {
    //see if admin exists
    //Encrypt password
    //return jsonwebtoken
    let user = await User.findOne({ email });

    if (user) {
    return res
          .status(400)
          .json({ errors: [{ msg: 'User already exists' }] });
  
        }  
        user = new User({
        name,
        gender,
        age,
        email,
        password,
        phoneNumber,
        isAdmin:false
      });

      const salt = await bcrypt.genSalt(10);

      user.password = await bcrypt.hash(password, salt);

      await user.save();

      const payload = {
        user: {
          id: user.id
        }
      };

      jwt.sign(
        payload,
        config.get('jwtToken'),
        { expiresIn: '5 days' },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server error');
    }
  }
);

module.exports = router;