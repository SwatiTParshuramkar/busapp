const express = require('express');
const router = express.Router();
const bcrypt=require('bcryptjs');
const jwt = require('jsonwebtoken')
const config=require('config')
const {check,validationResult } = require('express-validator');

const User =require('../../models/User')



// post api for user registration
router.post("/",
[
    // here we are checking name and email and password
    check("name","Name is required").not().isEmpty(),
    check("email","Please include a valid email..").isEmail(),
    check("password","please enter a password with 6 or more charchter" ).isLength({min:6}),
    check("phoneNumber","enter your mobile number").isLength(10),
    check("gender","please enter gender").isLength({min:4}),
    check("isAdmin","please enter isAdmin as true or false").not().isEmpty()
],
async (req,res)=>{
    // console.log(req.body)
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({errors:errors.array() })
    }
    
    const { name, email, password,phoneNumber,gender,isAdmin }=req.body;
    try {
        //  see if user exists 
        let user= await User.findOne({ email });

        if(user){
            res.status(400).json({errors: [{msg:'User already exist'}]})
        }
        user= new User({
            name,
            email,
            password,
            phoneNumber,
            gender,
            isAdmin
        });

        // encrypt password
        const salt=await bcrypt.genSalt(10)
        user.password=await bcrypt.hash(password,salt)
        await user.save(); 

        const payload = {
            user: { 
                id: user.id
            }
        };

        jwt.sign(
            payload,
            config.get('jwtSecret'),
            {expiresIn: '5 days'},
            (err, token) => {
                if (err) throw err;
                res.json({ token });
            }
        );
        res.send(' User registerd')
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Sever Error')
        
    }
  }
);

module.exports=router;