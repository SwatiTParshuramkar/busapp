const express = require("express");
const router = express.Router();
const config = require('config')
const {check, validationResult } = require("express-validator");

const Bus = require('../../models/Bus');
const admin = require("../../middleware/adminAuth");

// for creating bus

router.post('/',[admin,[
    check("name","Name is required").not().isEmpty(),
    check("numberOfSeats","please select a seat").not().isEmpty(),
    check("startCity","please enter your start city").not().isEmpty(),
    check("destinationCity","please eneter your destiantioncity").not().isEmpty(),
    check("description","please select AC or NonAc").not().isEmpty(),
]
],async(req,res)=>{
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({errors:errors.array() })
    }
    try {
        let bus = new Bus(req.body)
        const createbus = await bus.save()
        res.status(200).json({createbus})
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Sever Error')
    }
    })

    // search bus
router.get('/search', [
    check("startCity","startCity is required").not().isEmpty(),
    check("destinationCity","destinationcity should be required").not().isEmpty(),
    check("arrivaleDate","date is required").isDate()
],async(req, res)=>{
    const errors  = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(400).json({errors:errors.array() })
        }

        let from = req.query.startCity;
        let to = req.query.destinationCity;
        let date = req.query.arrivelDate
        try{
        let busData = await Bus.findOne({startCity: from, destinationCity: to,arrivalDate:date});

        if(!busData) 
        return res.status(400).json({msg:"Sorry no Buses available"});


        res.status(200).json(busData)
        // console.log(busData)
        }catch(err){
            console.error(err.message)
            res.json({ error: err })
        }
        
    });
//GET ALL Buses
//access PUBLIC

router.get("/",async(req,res)=>{
    try{
    const buses = await Bus.find();
    res.json(buses)

    }catch(err){
        console.error(err.message)
        res.status(500).send("Server -Error");
    }
});

// get bus by bus ID
router.get("/bus/:bus_id",async(req,res)=>{
    try{
       const bus=await Bus.findOne({
           bus:req.params.bus_id});

       if(!bus) 
       return res.status(400).json({msg:"There is no bus for this busID"});

       res.json(bus)

    }catch(err){
        console.error(err.message)
        // it is valid bus id 
        if(err.kind=="ObjectId"){
            return res.status(400).json({msg:"Bus not found"});

        }
        res.status(500).send("Server--Error");

    }
});
// update bus
router.put("/update/:id",admin, async (req, res) => {
    let bus = await Bus.findByIdAndUpdate(req.params.id, req.body, {new : true});

    return res.status(201).send({bus});
});



module.exports = router;