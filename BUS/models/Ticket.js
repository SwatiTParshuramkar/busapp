const mongoose = require('mongoose');

const TicketSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    phoneNumber:{
        type:Number,
        required:true
    },
    age:{
        type:Number,
        required:true
    },
    gender:{
        type:String,
        required:true
    },
    busName:{
        type:String,
        required:true
    },
    startCity:{
        type:String,
        required:true
    },
    endCity:{
        type:String,
        required:true
    },
    seatNo:{
        type:Number,
        required:true
    },
    costOfTicket:{
        type:Number,
        required:true
    },
    isBooked:{
        type:Boolean,
        required:true

    },
    // status:{
    //     type:String,
    //     required:true
    // }

})
module.exports = Ticket = mongoose.model('ticket', TicketSchema);
